<?php
// xml filename matching
$prefix = 'crcradioam';
$types = [
	'persons',
	'comp',
];
$version = 'v1';
$file = [];

// sql database
$dbFilename = 'db.sqlite';
$tableNames = [
	'persons' => 'people',
	'comp' => 'companies',
];
$tables = [
	'people' => [
		'ID' => 'integer',
		'Name' => 'text',
		'Class' => 'integer',
		'Address' => 'text',
		'ZipCode' => 'text',
		'City' => 'text',
		'Municipality' => 'text',
		'Area' => 'text',
		'Decision' => 'text',
		'DecisionDate' => 'text',
		'SilentKey' => 'integer',
	],
	'companies' => [
		'ID' => 'integer',
		'Name' => 'text',
		'CompanyType' => 'text',
		'Address' => 'text',
		'ZipCode' => 'text',
		'City' => 'text',
		'Municipality' => 'text',
		'Area' => 'text',
		'ResponsiblePersonID' => 'integer',
		'Responsible' => 'text',
	],
	'codes' => [
		'Code' => 'text',
		'CodeType' => 'text',
		'PersonID' => 'integer',
		'CompanyID' => 'integer',
		'Temporal' => 'integer',
		'StartDate' => 'text',
		'EndDate' => 'text',
	],
];

function fixString($value, $fixDashes = false) {
	$value = trim($value);
	$value = preg_replace('/\s/', ' ', $value);
	$value = preg_replace('/\s{2,}/', ' ', $value);
	
	if ($fixDashes) {
		$value = preg_replace('/(\S)\s+-/', '\1-', $value);
		$value = preg_replace('/-\s+(\S)/', '-\1', $value);
	}
	
	if (strlen($value) === 0) {
		return null;
	}
	
	return $value;
}

function formatValue($column, $value) {
	if ($value instanceof SimpleXMLElement && $value->count() === 0) {
		$value = null;
	}
	
	if (is_string($value)) {
		$value = fixString($value, in_array($column, ['FirstName', 'Name']));
	}
	elseif (is_array($value) && $column === 'Type' && count($value) === 2) {
		return array_merge(formatValue('CompanyType', $value[0]), formatValue('CodeType', $value[1]));
	}
	else {
		if (!is_null($value)) {
			throw new Exception('Invalid value for column ' . $column . ': ' . json_encode($value));
		}
	}
	
	switch ($column) {
		case 'FirstName':
			$column = 'Name';
			break;
		case 'Type':
			$column = 'CodeType';
			break;
	}
	
	if (is_null($value)) {
		return [
			$column => $value,
		];
	}
	
	switch ($column) {
		case 'Decision':
		case 'DecisionDate':
		case 'StartDate':
		case 'EndDate':
		case 'Name':
		case 'CodeType':
		case 'Responsible':
			return [
				$column => $value,
			];
		case 'Class':
			return [
				$column => intval($value),
			];
		case 'Code':
			$values = [];
			$parts = explode(',', $value);

			if (count($parts) === 2 && $parts[1] === 'sk') {
				$values['SilentKey'] = 1;
				$value = substr($value, 0, -3);
			}
			
			$values['Code'] = $value;
			
			return $values;
		case 'Temporal':
			return [
				$column => intval($value === 'Да'),
			];
		case 'CompanyType':
			return [
				$column => $value === '-' ? null : $value,
			];
		case 'Address':
		case 'Headquarters':
			if (preg_match('/^(?:(.*), )?(?:п\.к\.(\d+) )?(?:(?:гр|с).\s*(.*))?, Община:\s*(.*), Област:\s*(.*)$/', $value, $matches)) {
				return [
					'Address' => fixString($matches[1]),
					'ZipCode' => fixString($matches[2]),
					'City' => fixString($matches[3]),
					'Municipality' => fixString($matches[4]),
					'Area' => fixString($matches[5]),
				];
			}
			throw new Exception('Invalid address format: ' . $value);
		default:
			throw new Exception('Unknown column ' . $column);
	}
}

foreach ($types as $type) {
	$typePrefix = $prefix . $type . $version;
	$files = glob(__DIR__ . DIRECTORY_SEPARATOR . $typePrefix . '*.xml');
	$typeFiles = [];
	
	foreach ($files as $path) {
		$filename = pathinfo($path, PATHINFO_BASENAME);
		
		if (
			preg_match('/^' . $typePrefix . '(\d{8})\.xml$/', $filename, $matches) === false ||
			count($matches) !== 2
		) {
			continue;
		}
		
		$typeFiles[$matches[0]] = intval($matches[1]);
	}
	
	if (count($typeFiles) === 0) {
		throw new Exception('Cannot find file for type ' . $type);
	}

	end($typeFiles);
	$file[$type] = key($typeFiles);
}

$db = new PDO('sqlite:' . $dbFilename, null, null, [
	PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
]);

$columnNames = [];
$stmtInsert = [];

foreach ($tables as $tableName => $columns) {
	$columnNames[$tableName] = array_keys($columns);

	// drop
	$sql = 'DROP TABLE IF EXISTS ' . $tableName;
	$db->exec($sql);

	// create
	$sqlColumns = implode(', ', array_merge(array_map(function($k, $v) {
		return $k . ' ' . $v;
	}, $columnNames[$tableName], $columns), [
		'PRIMARY KEY ("' . reset($columnNames[$tableName]) . '")',
	]));
	$sql = 'CREATE TABLE IF NOT EXISTS ' . $tableName . ' (' . $sqlColumns . ')';
	$db->exec($sql);
	
	// prepare insert
	$index = array_search('ID', $columnNames[$tableName], true);

	if ($index !== false) {
		unset($columnNames[$tableName][$index]);
	}
}

foreach ($tables as $tableName => $columns) {
	$sql = 'INSERT INTO ' . $tableName . ' (' . implode(', ', $columnNames[$tableName]) . ') VALUES (:' . implode(', :', $columnNames[$tableName]) . ')';
	$stmtInsert[$tableName] = $db->prepare($sql);
}

$index = [
	'persons' => [],
	'comp' => [],
];

$seq = 0;

$db->beginTransaction();

foreach (['persons', 'comp'] as $type) {
	$tableName = $tableNames[$type];
	echo 'table: ', $tableName, PHP_EOL;
	
	$filename = $file[$type];
	$reader = simplexml_load_file($filename);
	$rows = $reader->{$type === 'persons' ? 'RadioAmatrur' : 'RadioAmateur'};
	foreach ($rows as $row) {
		$xmlRow = get_object_vars($row);
		$ownerRow = array_fill_keys($columnNames[$tableName], null);
		$codeRow = array_fill_keys($columnNames['codes'], null);
		foreach ($xmlRow as $xmlKey => $xmlValue) {
			$values = formatValue($xmlKey, $xmlValue);
			
			foreach ($values as $column => $value) {
				if (array_key_exists($column, $ownerRow)) {
					$ownerRow[$column] = $value;
				}
				elseif (array_key_exists($column, $codeRow)) {
					$codeRow[$column] = $value;
				}
				else {
					var_dump($xmlRow, $ownerRow, $codeRow, $xmlKey, $xmlValue, $values);
					throw new Exception('Column ' . $column . ' does not belong neither to owner nor to code');
				}
			}
		}
		
		if ($type === 'persons') {
			$indexKey = $ownerRow['Name'] . '$' . $ownerRow['Class'] . '$' . ($ownerRow['Decision'] ?? $seq++) . '$' . ($ownerRow['DecisionDate'] ?? $seq++);
			$ownerColumn = 'PersonID';
		}
		else {
			$indexKey = $ownerRow['Name'];
			$ownerColumn = 'CompanyID';
		}
		
		if (array_key_exists($indexKey, $index[$type])) {
			$insertId = $index[$type][$indexKey]['ID'];
		}
		else {
			$stmtInsert[$tableName]->execute($ownerRow);
			$insertId = $db->lastInsertId();
			$ownerRow['ID'] = $insertId;
			$index[$type][$indexKey] = $ownerRow;
		}
		
		$codeRow[$ownerColumn] = intval($insertId);
		$stmtInsert['codes']->execute($codeRow);
		echo '.';
	}
	
	echo PHP_EOL, 'complete', PHP_EOL;
}

$db->commit();
